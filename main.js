const page = new Vue ({
  el: '#page',
  data: {
    myName: 'Kevin Jones',
    headshot: './assets/profPic.jpg',
    links: {
      twitter: 'https://twitter.com/KevDJJones',
      linkedin: 'https://www.linkedin.com/in/kevinjones215',
      freecodecamp: 'https://www.freecodecamp.org/kaydeejay',
      gitlab: 'https://gitlab.com/KayDeeJay'
    }
  }
});
